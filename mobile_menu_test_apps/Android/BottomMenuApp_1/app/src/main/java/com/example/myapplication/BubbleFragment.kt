package com.example.myapplication

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.example.myapplication.databinding.BubbleFragmentBinding

class BubbleFragment : Fragment() {

    private var _binding :BubbleFragmentBinding? = null

    private val binding get() = _binding!!

    companion object {
        fun newInstance() = BubbleFragment()
    }

    private lateinit var viewModel: BubbleViewModel

    private var testNum : Int = 1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = BubbleFragmentBinding.inflate(inflater, container, false)

        viewModel = ViewModelProvider(this).get(BubbleViewModel::class.java)

        val root : View = binding.root

        val numberAfterSortTextField = binding.numberAfterSortTextView
        val sortButton : Button = binding.sortButton
        val textView2 = binding.textView2

        print("test0")

        textView2.text = "test1"

        sortButton.setOnClickListener {
            val numberToSortEditText = binding.numberToSortEditText
            viewModel.sort(numberToSortEditText.text.toString())
        }

        viewModel.numberAfterSort.observe(viewLifecycleOwner, {
            numberAfterSortTextField.text = it
        })

        return root;
    }

}