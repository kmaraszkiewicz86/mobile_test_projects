package com.example.myapplication

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class BubbleViewModel : ViewModel() {
    private val _numberAfterSort = MutableLiveData<String>().apply {
        value = "Type some numbers separate by comma and click sort button"
    }

    val numberAfterSort : LiveData<String> = _numberAfterSort

    fun sort(numberToSort: String) {

        if (numberToSort.isNullOrEmpty()) {
            return
        }

        val numberInString : List<String> = numberToSort.split(',');

        if (numberInString.count() == 0) {
            return
        }

        var numbers : MutableList<Int> = numberInString.map { it.toInt() }.toMutableList()

        if (numbers.count() == 0) {
            return
        }

        var shouldContinue : Boolean = true
        for (index in 0 until numbers.count()) {

            if (!shouldContinue) {
                break
            }

            shouldContinue = false

            for (outerIndex in 0 until  numbers.count() - 1) {
                if (numbers[outerIndex] > numbers[outerIndex + 1]) {
                    swap(numbers, outerIndex, outerIndex + 1)

                    shouldContinue = true
                }
            }

            _numberAfterSort.value = numbers.joinToString()
        }
    }

    private fun swap(number: MutableList<Int>, index1 : Int, index2 : Int) {
        var tmpValue = number[index1]
        number[index1] = number[index2]
        number[index2] = tmpValue
    }
}