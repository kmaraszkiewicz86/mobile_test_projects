package com.example.myapplication

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {

    private  val _test = MutableLiveData<String>().apply {
        value = "To jest testowa wiadomość"
    }

    val testValue : LiveData<String> = _test


    fun changeText(testValue: String) {
        _test.value = testValue
    }
}