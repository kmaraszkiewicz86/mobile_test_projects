package com.example.myapplication.insertionsort

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.myapplication.R
import com.example.myapplication.databinding.InsertionSortFragmentBinding

class InsertionSortFragment : Fragment() {

    companion object {
        fun newInstance() = InsertionSortFragment()
    }

    private var _binding : InsertionSortFragmentBinding? = null

    private val binding get() = _binding!!

    private lateinit var viewModel: InsertionSortViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = InsertionSortFragmentBinding.inflate(inflater, container, false)

        val root = binding.root

        viewModel = ViewModelProvider(this).get(InsertionSortViewModel::class.java)

        val valueForSortEditText = binding.valueForSortEdittext
        val resultTextView = binding.resultTextView
        val sortButton = binding.insertionSortButton

        viewModel.numberAfterSort.observe(viewLifecycleOwner) {
            resultTextView.text = it
        }

        sortButton.setOnClickListener() {
            viewModel.sort(valueForSortEditText.text.toString())
        }

        return root
    }

}