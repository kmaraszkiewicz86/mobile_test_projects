package com.example.myapplication.insertionsort

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class InsertionSortViewModel : ViewModel() {
    private val _numbersAfterSort = MutableLiveData<String>().apply {
        value = "Haven't got any result yet"
    }

    public val numberAfterSort : LiveData<String> = _numbersAfterSort

    public fun sort(numbersToSort : String) {

        if (numbersToSort.isNullOrEmpty()) {
            return
        }

        var numbersIntMutableList = numbersToSort.split(',').map { it.toInt() }.toMutableList();

        for (index in 0 until numbersIntMutableList.count()) {
            var minValueIndex = index

            for (outerIndex in index until numbersIntMutableList.count()) {
                if (numbersIntMutableList[outerIndex] < numbersIntMutableList[minValueIndex]) {
                    minValueIndex = outerIndex
                }
            }

            swap(numbersIntMutableList, index, minValueIndex)
        }

        _numbersAfterSort.value = numbersIntMutableList.joinToString()
    }

    private fun swap(numbers: MutableList<Int>, index1: Int, index2: Int) {
        val tmp = numbers[index1]
        numbers[index1] = numbers[index2]
        numbers[index2] = tmp
    }
}