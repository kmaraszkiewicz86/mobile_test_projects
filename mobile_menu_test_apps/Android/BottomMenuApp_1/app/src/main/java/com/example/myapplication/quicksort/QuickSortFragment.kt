package com.example.myapplication.quicksort

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.myapplication.R
import com.example.myapplication.databinding.QuickSortFragmentBinding

class QuickSortFragment : Fragment() {

    companion object {
        fun newInstance() = QuickSortFragment()
    }

    private var _binding: QuickSortFragmentBinding? = null

    private val binding get() = _binding!!

    private lateinit var viewModel: QuickSortViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewModel = ViewModelProvider(this).get(QuickSortViewModel::class.java)
        _binding = QuickSortFragmentBinding.inflate(inflater, container, false)
        val root = binding.root

        val numberToSortEditText = binding.numberToSortEditText
        val sortButton = binding.sortButton
        val resultTextView = binding.resultTextView

        viewModel.numberAfterSort.observe(viewLifecycleOwner) {
            resultTextView.text = it
        }

        sortButton.setOnClickListener() {
            viewModel.sort(numberToSortEditText.text.toString())
        }

        return root
    }

}