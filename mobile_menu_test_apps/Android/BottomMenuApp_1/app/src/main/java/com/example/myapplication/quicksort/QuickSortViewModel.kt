package com.example.myapplication.quicksort

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class QuickSortViewModel : ViewModel() {
    val _numberAfterSort = MutableLiveData<String>().apply {
        value = "Type numbers to sort"
    }

    val numberAfterSort : LiveData<String> = _numberAfterSort

    fun sort(numberToSort: String) {
        if (numberToSort.isNullOrEmpty()) {
            return
        }

        var numberInArray = numberToSort.split(',').map { it.toInt() }.toMutableList()

        sort(numberInArray, 0, numberInArray.count() - 1)

        _numberAfterSort.value = numberInArray.joinToString()
    }

    private fun sort(numbers : MutableList<Int>, left: Int, right: Int) {
        if (left > right) {
            return
        }

        var pivot = numbers[right]
        var border = left - 1
        var index = left

        while (index < right) {
            if (numbers[index] < pivot) {
                border += 1
                if (border != index) {
                    swap(numbers, index, border)
                }
            }

            index += 1
        }

        border++
        if (border < right) {
            swap(numbers, border, right)
        }

        sort(numbers, border + 1, right)
        sort(numbers, left, border - 1)
    }

    private fun swap(numbers: MutableList<Int>, index1: Int, index2: Int) {
        val tmp = numbers[index1]
        numbers[index1] = numbers[index2]
        numbers[index2] = tmp
    }
}