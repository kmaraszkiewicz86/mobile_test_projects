//
//  BubbleSortViewModel.swift
//  BottomMenuApp
//
//  Created by Krzysztof Maraszkiewicz on 30/11/2021.
//

import SwiftUI

class BubbleSortViewModel : ObservableObject {
    @Published var numbersToSort = ""
    
    @Published var numbersAfterSort = "Type numbers to sort and click the sort button"
    
    func sort() {
        var numberInArray = numbersToSort.components(separatedBy: ",").map { val in
            Int(val)!
        }
        
        var shouldContinue = true
        
        for _ in 0..<numberInArray.count {
            if (!shouldContinue) {
                break
            }
            
            shouldContinue = false
            
            for outerIndex in 0..<numberInArray.count - 1 {
                if (numberInArray[outerIndex] > numberInArray[outerIndex + 1]) {
                    swap(numbers: &numberInArray, index1: outerIndex, index2: outerIndex + 1)
                    shouldContinue = true
                }
            }
        }
        
        numbersAfterSort = numberInArray.map({ val in
            String(val)
        }).joined(separator: ",")
    }
    
    private func swap(numbers: inout [Int], index1: Int, index2: Int) {
        let tmp = numbers[index1]
        numbers[index1] = numbers[index2]
        numbers[index2] = tmp
    }
}
