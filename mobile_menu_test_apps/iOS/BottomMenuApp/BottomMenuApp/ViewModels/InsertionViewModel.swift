//
//  InsertionViewModel.swift
//  BottomMenuApp
//
//  Created by Krzysztof Maraszkiewicz on 30/11/2021.
//

import SwiftUI

class InsertionViewModel : ObservableObject {
    @Published var numberToSort: String = ""
    
    @Published var numberAfterSort: String = "Type numbers and click sort button"
    
    func sort() {
        var numbersInArray = numberToSort.components(separatedBy: ",").map { val in
            Int(val)!
        }
        
        if (numbersInArray.count <= 0) {
            return
        }
        
        for i in 0..<numbersInArray.count {
            var minIndex = i
            for outerIndex in i..<numbersInArray.count {
                if (numbersInArray[minIndex] > numbersInArray[outerIndex]) {
                    minIndex = outerIndex
                }
            }
            
            swap(numbers: &numbersInArray, index1: minIndex, index2: i)
        }
        
        numberAfterSort = numbersInArray.map({ val in
            String(val)
        }).joined(separator: ", ")
    }
    
    private func swap(numbers: inout [Int], index1: Int, index2: Int) {
        let tmp = numbers[index2]
        numbers[index2] = numbers[index1]
        numbers[index1] = tmp
    }
    
}
