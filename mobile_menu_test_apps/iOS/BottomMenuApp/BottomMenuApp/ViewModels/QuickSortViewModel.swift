//
//  QuickSortViewModel.swift
//  BottomMenuApp
//
//  Created by Krzysztof Maraszkiewicz on 30/11/2021.
//

import SwiftUI

class QuickSortViewModel : ObservableObject {
    
    @Published var numbersToSort = ""
    
    @Published var numbersAfterSort = "Type numbers and click the sort button"
    
    func sort() {
        var numbersInArray = numbersToSort.components(separatedBy: ",").map { val in
            Int(val)!
        }
        
        sort(numbers: &numbersInArray, left: 0, right: numbersInArray.count - 1)
        
        numbersAfterSort = numbersInArray.map({ val in
            String(val)
        }).joined(separator: ",")
    }
    
    private func sort(numbers: inout [Int], left: Int, right: Int) {
        if left > right {
            return
        }
        
        let pivot = numbers[right]
        var border = left - 1
        var index = left
        
        while (index < right) {
            if (pivot > numbers[index]) {
                border += 1
                if border != index {
                    swap(numbers: &numbers, index1: border, index2: index)
                }
            }
            
            index += 1
        }
        
        border += 1
        if border < right {
            swap(numbers: &numbers, index1: border, index2: right)
        }
        
        sort(numbers: &numbers, left: left, right: border - 1)
        sort(numbers: &numbers, left: border + 1, right: right)
    }
    
    private func swap(numbers: inout [Int], index1: Int, index2: Int) {
        let tmp = numbers[index1]
        numbers[index1] = numbers[index2]
        numbers[index2] = tmp
    }
}
