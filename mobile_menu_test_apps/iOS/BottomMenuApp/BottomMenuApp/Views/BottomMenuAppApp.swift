//
//  BottomMenuAppApp.swift
//  BottomMenuApp
//
//  Created by Krzysztof Maraszkiewicz on 30/11/2021.
//

import SwiftUI

@main
struct BottomMenuAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
