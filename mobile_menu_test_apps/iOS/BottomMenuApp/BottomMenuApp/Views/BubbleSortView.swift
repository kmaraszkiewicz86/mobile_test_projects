//
//  BubbleSortView.swift
//  BottomMenuApp
//
//  Created by Krzysztof Maraszkiewicz on 30/11/2021.
//

import SwiftUI

struct BubbleSortView: View {
    
    @ObservedObject var viewModel : BubbleSortViewModel
    
    var body: some View {
        VStack {
            TextField("Type numbers to sort", text: $viewModel.numbersToSort)
                .textFieldStyle(.roundedBorder)
                .padding(5)
            
            Spacer()
            
            Text(viewModel.numbersAfterSort)
                .padding(5)
            
            Spacer()
            
            Button {
                viewModel.sort()
            } label: {
                Text("sort")
                    .foregroundColor(.white)
            }
            .frame(minWidth: 0, maxWidth: .infinity, minHeight: 50)
            .background(.blue)
            .padding(20)

        }
    }
}

struct BubbleSortView_Previews: PreviewProvider {
    static var previews: some View {
        BubbleSortView(viewModel: BubbleSortViewModel())
    }
}
