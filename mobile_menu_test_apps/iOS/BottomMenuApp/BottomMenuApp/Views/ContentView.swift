//
//  ContentView.swift
//  BottomMenuApp
//
//  Created by Krzysztof Maraszkiewicz on 30/11/2021.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        TabView {
            InsertionSortView(viewModel: InsertionViewModel())
                .tabItem {
                    Text("Insertion Sort")
                }
            BubbleSortView(viewModel: BubbleSortViewModel())
                .tabItem {
                    Text("Bubble sort")
                }
            QuickSortView(viewModel: QuickSortViewModel())
                .tabItem {
                    Text("Quick sort")
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
