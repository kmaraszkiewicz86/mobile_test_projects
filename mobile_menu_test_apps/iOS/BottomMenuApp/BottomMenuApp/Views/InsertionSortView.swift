//
//  InsertionSortView.swift
//  BottomMenuApp
//
//  Created by Krzysztof Maraszkiewicz on 30/11/2021.
//

import SwiftUI

struct InsertionSortView: View {
    
    @ObservedObject var viewModel : InsertionViewModel
    
    var body: some View {
        VStack {
            TextField("Type numbers to sort", text: $viewModel.numberToSort)
                .textFieldStyle(.roundedBorder)
                .padding(10)
                
            Spacer()
            
            Text(viewModel.numberAfterSort)
                .padding(10)
            Spacer()
            
            Button(action: {
                viewModel.sort()
            }) {
                Text("Sort")
                    .foregroundColor(.white)
            }
            .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 50, alignment: .center)
            .background(.blue)
            .padding(20)
        }
    }
}

struct InsertionSortView_Previews: PreviewProvider {
    static var previews: some View {
        InsertionSortView(viewModel: InsertionViewModel())
.previewInterfaceOrientation(.portrait)
    }
}
