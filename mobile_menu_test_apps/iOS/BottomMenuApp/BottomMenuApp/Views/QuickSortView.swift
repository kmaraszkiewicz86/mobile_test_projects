//
//  QuickSortView.swift
//  BottomMenuApp
//
//  Created by Krzysztof Maraszkiewicz on 30/11/2021.
//

import SwiftUI

struct QuickSortView: View {
    
    @ObservedObject var viewModel : QuickSortViewModel
    
    var body: some View {
        VStack {
            TextField("Type numer to sort", text: $viewModel.numbersToSort)
                .textFieldStyle(.roundedBorder)
                .padding(5)
            
            Spacer()
            
            Text(viewModel.numbersAfterSort)
                .padding(5)
            
            Spacer()
            
            Button {
                viewModel.sort()
            } label: {
                Text("Sort")
                    .foregroundColor(.white)
            }
            .frame(minWidth: 0, maxWidth: .infinity, minHeight: 50)
            .background(.blue)
            .padding(20)
        }
    }
}

struct QuickSortView_Previews: PreviewProvider {
    static var previews: some View {
        QuickSortView(viewModel: QuickSortViewModel())
    }
}
